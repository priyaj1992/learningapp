import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { Button, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AppButton from './app/components/AppButton';

export default function App() {
  const [item, setItem] = useState(0);

  
  return (
    <>
    <View style={customStyle.appNameContainer}>
      <Image  style={customStyle.appLogo} source={require('./assets/sunny.png')}/>
      <Text style={customStyle.appName}>Gyaanoday</Text>
    </View>
    <View style={customStyle.container}>
      <View style={customStyle.btnView}>
        <AppButton
            onPress = {() => alert("Login")}
            text="Login"
            color="tomato"
            width="48%"
        />
        <AppButton
            onPress = {() => alert("Sign Up")}
            text="Sign Up"
            color="dodgerblue"
            width="48%"
        />
      </View>
    </View>
    </>
  );
}

const customStyle = StyleSheet.create({
  appLogo: {
    height: 100,
    width: 100
  },
  appNameContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginVertical: "70%"
  },  
  appName: {
    fontSize: 40,
    fontWeight: "600",
    alignItems: "center",
    justifyContent: "center"
  },
  btnView: {
    flexDirection: "row"
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-end',
  }
});
