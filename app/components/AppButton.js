import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

export default function AppButton({color , text , onPress, width = "97%"}) {
    return (
        <TouchableOpacity style={[styles.customBtn, {backgroundColor: color, width: width}]} onPress={onPress}>
            <View>
                <Text style={styles.btnText}>{text}</Text>
            </View>
        </TouchableOpacity>

    )
}

const styles = StyleSheet.create({
    btnText: {
        color: "white",
        fontSize: 18,
      },
      customBtn:{
        padding: 15,
        borderRadius: 10,
        alignItems: "center",
        marginVertical: 10,
        marginHorizontal: 4
      },
})
